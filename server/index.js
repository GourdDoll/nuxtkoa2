import Koa from 'koa'
import { Nuxt, Builder } from 'nuxt'
import bodyparser from 'koa-bodyparser'
import city from './interface/city.js'
import user from './interface/user.js'
import session from 'koa-generic-session'
import redis from 'koa-redis'
    // "dev": "backpack dev",


async function start () {
  const app = new Koa()
  const host = process.env.HOST || '0.0.0.0'
  const port = process.env.PORT || 8000

	// middlewares
	app.use(bodyparser({
	  enableTypes:['json', 'form', 'text'] 
	}));
	
  // Import and Set Nuxt.js options
  const config = require('../nuxt.config.js')
  config.dev = !(app.env === 'production')

  // Instantiate nuxt.js
  const nuxt = new Nuxt(config)

  // Build in development
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }
	
	// session 配置
	// app.keys = ['Lihan#_123'];
	// app.use(session({
	// 	// 配置cookie
	// 	cookie:{
	// 		paht: '/',
	// 		httpOnly: true,
	// 		maxAge: 24*60*60*1000
	// 	},
	// 	// 配置redis
	// 	store:redis({
	// 		all: '127.0.0.1:6379',
	// 	})
	// }));
	
	// 引入接口路由
	app.use(city.routes()).use(city.allowedMethods())
	app.use(user.routes()).use(user.allowedMethods())
	
  app.use(ctx => {
    ctx.status = 200
    ctx.respond = false // Mark request as handled for Koa
    ctx.req.ctx = ctx // This might be useful later on, e.g. in nuxtServerInit or with nuxt-stash
    nuxt.render(ctx.req, ctx.res)
  })

  app.listen(port, host)
  console.log('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
}

start()
