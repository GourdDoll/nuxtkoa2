export default {
	dbs:'mongodb://127.0.0.1:27017/student',
	redis:{
		get host(){
			return '127.0.0.1'
		},
		get port(){
			return 6379
		}
	},
	smtp:{
		get host(){
			return 'smtp.qq.com'
		},
		get user(){
			return '542356155@qq.com'
		},
		get pass(){
			return 'qq邮箱授权码'
		},
		get code(){
			return ()=>{
				return Math.random().toString(16).slice(2,6).toUpperCase()
			}
		},
		get expire(){
			return ()=>{
				return (+new Date() +60 * 1000)
			}
		}
	}
}