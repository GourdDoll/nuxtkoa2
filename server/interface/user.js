// 注册接口
import User from '../dbs/models/user.js'
import Router from 'koa-router'
import Redis from 'koa-redis'
import Nodemailer from 'nodemailer'
import Passport from './utils/passport.js'

import Email from '../dbs/config.js'

let router = new Router({ // 路由
	prefix: '/users'
})


let redisClient = new Redis().client // reids


router.post('/signup', async (ctx) => {
	let {
		username,
		passwrod,
		phone,
		email,
		code
	} = ctx.request.body
	if (code) {
		const saveCode = await redisClient.hget(`nodemail:${username}`, 'code')
		const saveExpire = await redisClient.hget(`nodemail:${username}`, 'expire')
		if (code === saveCode) {
			if (+new Date() < saveExpire) {
				ctx.body = {
					code: -1,
					msg: '验证码已过期'
				}
				return false
			}
		} else {
			ctx.body = {
				code: -1,
				msg: '验证码错误'
			}
			return false
		}
	} else {
		ctx.body = {
			code: -1,
			msg: '请输入验证码'
		}
		return false
	}

	let user = await User.find({
		username
	})
	if (user.length) { // 如果数据库中找到username  说明已经注册过
		ctx.body = {
			code: -1,
			msg: '请勿重复注册'
		}
		return false
	}

	// 进行写库操作
	let newUser = await User.create({
		username,
		password,
		email,
		phone
	})
	// 如果写入成功 就登录
	if (newUser) {
		let res = await this.$axios.post('/users/signin', {
			username,
			password
		})
		if (res.data && res.data.code == 0) {
			ctx.body = {
				code: 0,
				msg: '注册成功',
				user: res.data.user
			}
		} else {
			ctx.body = {
				code: -1,
				msg: 'error'
			}
		}
	} else {
		ctx.body = {
			code: -1,
			msg: '注册失败'
		}
	}
})

router.post('/signin', async (ctx,next) => {
	return Passport.authenticate('local', (err, user, info, status) => {
		if (err) {
			ctx.body = {
				code: -1,
				msg: err
			}
		} else {
			if (user) {
				ctx.body = {
					code: 0,
					msg: '登陆成功',
					user
				}
				return ctx.login(user)
			} else {
				ctx.body = {
					code: 1,
					msg: info
				}
			}
		}
	})(ctx,next)
})

// 验证码
router.post('/code',async (ctx,next)=>{
	let username = ctx.request.body.username
	const saveExpire = await redisClient.hget(`nodemail:${username}`,'expire')
	console.log(saveExpire);
	if(saveExpire && +new Date() < saveExpire){
		ctx.body = {
			code:-1,
			msg:'验证请求过于频繁'
		}
		return false
	}
	// 发送邮件
	let transporter = Nodemailer.createTransport({
		host:Email.smtp.host,
		port:587,
		secure: false,
		auth:{
			user:Email.smtp.user,
			pass:Email.smtp.pass
		}
	})
	let ko = {
		code: Email.smtp.code(),
		expire: Email.smtp.expire(),
		email: ctx.request.body.email,
		user: ctx.request.body.username
	}
	let emailOpt = {
		from: `“认证邮件”<${Email.smtp.user}>`,
		to: ko.email,
		subject: '模拟美团网的注册码',
		html: `您在模拟美团网的注册码是${ko.code}`
	}
	await transporter.sendMail(emailOpt,(err,info)=>{
		if(err){
			return console.log('邮件发送失败');
		}else{
			redisClient.hmset(`nodemail:${ko.user}`,'code',ko.code,'expire',ko.expire,'email',ko.email)
		}
	})
	ctx.body={
		code:0,
		msg:'验证码已发送，可能会有延时，有效期1分钟'
	}
	
})

export default router
