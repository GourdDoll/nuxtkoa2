require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("koa-router");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("koa-redis");

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_koa__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_nuxt__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_nuxt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_nuxt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_koa_bodyparser__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_koa_bodyparser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_koa_bodyparser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__interface_city_js__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__interface_user_js__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_koa_generic_session__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_koa_generic_session___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_koa_generic_session__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_koa_redis__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_koa_redis___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_koa_redis__);







// "dev": "backpack dev",


async function start() {
		const app = new __WEBPACK_IMPORTED_MODULE_0_koa___default.a();
		const host = process.env.HOST || '0.0.0.0';
		const port = process.env.PORT || 8000;

		// middlewares
		app.use(__WEBPACK_IMPORTED_MODULE_2_koa_bodyparser___default()({
				enableTypes: ['json', 'form', 'text']
		}));

		// Import and Set Nuxt.js options
		const config = __webpack_require__(13);
		config.dev = !(app.env === 'production');

		// Instantiate nuxt.js
		const nuxt = new __WEBPACK_IMPORTED_MODULE_1_nuxt__["Nuxt"](config);

		// Build in development
		if (config.dev) {
				const builder = new __WEBPACK_IMPORTED_MODULE_1_nuxt__["Builder"](nuxt);
				await builder.build();
		}

		// session 配置
		// app.keys = ['Lihan#_123'];
		// app.use(session({
		// 	// 配置cookie
		// 	cookie:{
		// 		paht: '/',
		// 		httpOnly: true,
		// 		maxAge: 24*60*60*1000
		// 	},
		// 	// 配置redis
		// 	store:redis({
		// 		all: '127.0.0.1:6379',
		// 	})
		// }));

		// 引入接口路由
		app.use(__WEBPACK_IMPORTED_MODULE_3__interface_city_js__["a" /* default */].routes()).use(__WEBPACK_IMPORTED_MODULE_3__interface_city_js__["a" /* default */].allowedMethods());
		app.use(__WEBPACK_IMPORTED_MODULE_4__interface_user_js__["a" /* default */].routes()).use(__WEBPACK_IMPORTED_MODULE_4__interface_user_js__["a" /* default */].allowedMethods());

		app.use(ctx => {
				ctx.status = 200;
				ctx.respond = false; // Mark request as handled for Koa
				ctx.req.ctx = ctx; // This might be useful later on, e.g. in nuxtServerInit or with nuxt-stash
				nuxt.render(ctx.req, ctx.res);
		});

		app.listen(port, host);
		console.log('Server listening on ' + host + ':' + port); // eslint-disable-line no-console
}

start();

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("koa");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("nuxt");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("koa-bodyparser");

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa_router__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa_router___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_koa_router__);


const router = new __WEBPACK_IMPORTED_MODULE_0_koa_router___default.a({
	// prefix:'city'
});

router.get('/city', async ctx => {
	ctx.body = {
		data: ['aasd', 'sdafdsf']
	};
});

/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dbs_models_user_js__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_koa_router__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_koa_router___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_koa_router__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_koa_redis__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_koa_redis___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_koa_redis__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_nodemailer__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_nodemailer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_nodemailer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_passport_js__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dbs_config_js__ = __webpack_require__(11);
var _this = this;

// 注册接口








let router = new __WEBPACK_IMPORTED_MODULE_1_koa_router___default.a({ // 路由
	prefix: '/users'
});

let redisClient = new __WEBPACK_IMPORTED_MODULE_2_koa_redis___default.a().client; // reids


router.post('/signup', async ctx => {
	let {
		username,
		passwrod,
		phone,
		email,
		code
	} = ctx.request.body;
	if (code) {
		const saveCode = await redisClient.hget(`nodemail:${username}`, 'code');
		const saveExpire = await redisClient.hget(`nodemail:${username}`, 'expire');
		if (code === saveCode) {
			if (+new Date() < saveExpire) {
				ctx.body = {
					code: -1,
					msg: '验证码已过期'
				};
				return false;
			}
		} else {
			ctx.body = {
				code: -1,
				msg: '验证码错误'
			};
			return false;
		}
	} else {
		ctx.body = {
			code: -1,
			msg: '请输入验证码'
		};
		return false;
	}

	let user = await __WEBPACK_IMPORTED_MODULE_0__dbs_models_user_js__["a" /* default */].find({
		username
	});
	if (user.length) {
		// 如果数据库中找到username  说明已经注册过
		ctx.body = {
			code: -1,
			msg: '请勿重复注册'
		};
		return false;
	}

	// 进行写库操作
	let newUser = await __WEBPACK_IMPORTED_MODULE_0__dbs_models_user_js__["a" /* default */].create({
		username,
		password,
		email,
		phone
	});
	// 如果写入成功 就登录
	if (newUser) {
		let res = await _this.$axios.post('/users/signin', {
			username,
			password
		});
		if (res.data && res.data.code == 0) {
			ctx.body = {
				code: 0,
				msg: '注册成功',
				user: res.data.user
			};
		} else {
			ctx.body = {
				code: -1,
				msg: 'error'
			};
		}
	} else {
		ctx.body = {
			code: -1,
			msg: '注册失败'
		};
	}
});

router.post('/signin', async (ctx, next) => {
	return __WEBPACK_IMPORTED_MODULE_4__utils_passport_js__["a" /* default */].authenticate('local', (err, user, info, status) => {
		if (err) {
			ctx.body = {
				code: -1,
				msg: err
			};
		} else {
			if (user) {
				ctx.body = {
					code: 0,
					msg: '登陆成功',
					user
				};
				return ctx.login(user);
			} else {
				ctx.body = {
					code: 1,
					msg: info
				};
			}
		}
	})(ctx, next);
});

// 验证码
router.post('/code', async (ctx, next) => {
	let username = ctx.request.body.username;
	const saveExpire = await redisClient.hget(`nodemail:${username}`, 'expire');
	console.log(saveExpire);
	if (saveExpire && +new Date() < saveExpire) {
		ctx.body = {
			code: -1,
			msg: '验证请求过于频繁'
		};
		return false;
	}
	// 发送邮件
	let transporter = __WEBPACK_IMPORTED_MODULE_3_nodemailer___default.a.createTransport({
		host: __WEBPACK_IMPORTED_MODULE_5__dbs_config_js__["a" /* default */].smtp.host,
		port: 587,
		secure: false,
		auth: {
			user: __WEBPACK_IMPORTED_MODULE_5__dbs_config_js__["a" /* default */].smtp.user,
			pass: __WEBPACK_IMPORTED_MODULE_5__dbs_config_js__["a" /* default */].smtp.pass
		}
	});
	let ko = {
		code: __WEBPACK_IMPORTED_MODULE_5__dbs_config_js__["a" /* default */].smtp.code(),
		expire: __WEBPACK_IMPORTED_MODULE_5__dbs_config_js__["a" /* default */].smtp.expire(),
		email: ctx.request.body.email,
		user: ctx.request.body.username
	};
	let emailOpt = {
		from: `“认证邮件”<${__WEBPACK_IMPORTED_MODULE_5__dbs_config_js__["a" /* default */].smtp.user}>`,
		to: ko.email,
		subject: '模拟美团网的注册码',
		html: `您在模拟美团网的注册码是${ko.code}`
	};
	await transporter.sendMail(emailOpt, (err, info) => {
		if (err) {
			return console.log('邮件发送失败');
		} else {
			redisClient.hmset(`nodemail:${ko.user}`, 'code', ko.code, 'expire', ko.expire, 'email', ko.email);
		}
	});
	ctx.body = {
		code: 0,
		msg: '验证码已发送，可能会有延时，有效期1分钟'
	};
});

/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_mongoose__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_mongoose___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_mongoose__);

const Schema = __WEBPACK_IMPORTED_MODULE_0_mongoose___default.a.Schema;
const UserSchema = new Schema({
	username: {
		type: String,
		unique: true,
		require: true
	},
	password: {
		type: String,
		require: true
	},
	email: {
		type: String,
		require: true
	}
});

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0_mongoose___default.a.model('user', UserSchema));

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("nodemailer");

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
	dbs: 'mongodb://127.0.0.1:27017/student',
	redis: {
		get host() {
			return '127.0.0.1';
		},
		get port() {
			return 6379;
		}
	},
	smtp: {
		get host() {
			return 'smtp.qq.com';
		},
		get user() {
			return '542356155@qq.com';
		},
		get pass() {
			return 'lfszlvvbgxqvbdbb';
		},
		get code() {
			return () => {
				return Math.random().toString(16).slice(2, 6).toUpperCase();
			};
		},
		get expire() {
			return () => {
				return +new Date() + 60 * 1000;
			};
		}
	}
});

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("koa-generic-session");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = {
  server: {
    port: 8000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'starter',
    meta: [{ charset: 'utf-8' }, { name: 'viewport', content: 'width=device-width, initial-scale=1' }, { hid: 'description', name: 'description', content: 'Nuxt.js project' }],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Global CSS
  */
  css: ['~assets/css/reset.css', 'element-ui/lib/theme-chalk/reset.css', 'element-ui/lib/theme-chalk/index.css', '~assets/css/main.scss'],
  plugins: [{ src: './plugins/element-ui', ssr: true }],
  modules: ['@nuxtjs/axios'],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLINT on save
     */
    transpile: [/^element-ui/],
    extend(config, ctx) {
      if (ctx.Client) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    },
    cache: false
  }
};

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa_passport__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa_passport___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_koa_passport__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_passport_local__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_passport_local___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_passport_local__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dbs_models_user_js__ = __webpack_require__(8);




__WEBPACK_IMPORTED_MODULE_0_koa_passport___default.a.use(new __WEBPACK_IMPORTED_MODULE_1_passport_local___default.a(async (username, password, done) => {
	let where = {
		username
	};
	let result = await __WEBPACK_IMPORTED_MODULE_2__dbs_models_user_js__["a" /* default */].findOne(where);
	if (result != null) {
		if (result.password === password) {
			return done(null, result);
		} else {
			return done(null, false, '密码错误');
		}
	} else {
		return done(null, false, '用户不存在');
	}
}));

__WEBPACK_IMPORTED_MODULE_0_koa_passport___default.a.serializeUser((user, done) => {
	done(null, user);
});
__WEBPACK_IMPORTED_MODULE_0_koa_passport___default.a.deserializeUser((user, done) => {
	return done(null, user);
});

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0_koa_passport___default.a);

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("koa-passport");

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = require("passport-local");

/***/ })
/******/ ]);
//# sourceMappingURL=main.map